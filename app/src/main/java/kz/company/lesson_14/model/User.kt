package kz.company.lesson_14.model

data class User(
    val name: String,
    val email: String
)

class UserConverter {
    fun convert(hashMap: HashMap<String, String>): User =
        User(hashMap["name"] ?: "", hashMap["email"] ?: "")
}